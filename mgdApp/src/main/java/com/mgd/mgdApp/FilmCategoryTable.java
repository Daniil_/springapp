package com.mgd.mgdApp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="film_category")
public class FilmCategoryTable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer filmId;
    @Column(name = "category_id")
    private Integer categoryId;
    @Column(name="last_update")
    private Date lastUpdate;

    public void setFilmId(Integer filmId) {
        this.filmId = filmId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Integer getFilmId() {
        return filmId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }


}
