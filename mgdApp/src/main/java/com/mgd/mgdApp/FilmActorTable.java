package com.mgd.mgdApp;


import javax.persistence.*;
import java.util.Date;

@Entity
public class FilmActorTable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer actorId;
    @Column(name="fild_id")
    private Integer filmId;
    @Column(name="last_update")
    private Date lastUpdate;

    public void setActorId(Integer actorId) {
        this.actorId = actorId;
    }

    public void setFilmId(Integer filmId) {
        this.filmId = filmId;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Integer getActorId() {
        return actorId;
    }

    public Integer getFilmId() {
        return filmId;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }
}
