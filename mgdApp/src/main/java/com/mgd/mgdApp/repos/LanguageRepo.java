package com.mgd.mgdApp.repos;

import com.mgd.mgdApp.LanguageTable;
import org.springframework.data.repository.CrudRepository;

public interface LanguageRepo extends CrudRepository<LanguageTable, Integer> {


}
