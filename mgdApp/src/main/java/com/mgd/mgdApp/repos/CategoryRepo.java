package com.mgd.mgdApp.repos;

import com.mgd.mgdApp.CategoryTable;
import org.springframework.data.repository.CrudRepository;

public interface CategoryRepo extends CrudRepository<CategoryTable, Integer> {
}
