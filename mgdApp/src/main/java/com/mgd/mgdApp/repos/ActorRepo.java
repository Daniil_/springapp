package com.mgd.mgdApp.repos;

import com.mgd.mgdApp.ActorTable;
import org.springframework.data.repository.CrudRepository;

public interface ActorRepo extends CrudRepository<ActorTable,Integer> {
}
