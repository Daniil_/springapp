package com.mgd.mgdApp.repos;

import com.mgd.mgdApp.FilmActorTable;
import org.springframework.data.repository.CrudRepository;

public interface FilmActorRepo extends CrudRepository<FilmActorTable,Integer> {
}
